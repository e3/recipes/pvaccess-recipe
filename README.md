# pvAccess conda recipe

Home: https://github.com/epics-base/pvAccessCPP

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: pvAccess EPICS V4 C++ module
